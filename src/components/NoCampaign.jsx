import React from 'react'

const NoCampaign = (props) => {
  return (
    <div className="mt-24 mb-52 mx-auto w-full">
        <div className="flex-col text-center mt-8 mx-auto">
          <h1 className="text-tosca mb-2 text-2xl lg:text-4xl">Sorry, there are no campaign for '{props.title}'</h1>
        </div>
    </div>
  )
}

export default NoCampaign;