import React from "react";

export default function CampaignStory(props) {
  const splitStory = props.campaignData.story.split('. ')
  return (
    <div className="bg-gray-50 lg:mt-14 lg:py-8 lg:px-32 py-3 px-6 mt-6 rounded-md ">
      <h2 className="font-semibold lg:my-3 my-1 lg:text-lg text-base">The Story</h2>
      {splitStory.map((story, index) =>(
      <p key={index} className="text-justify lg:text-lg text-base">
         &nbsp; &nbsp;{story}.
        <br/>
      </p>
      ))}
    </div>
  );
}
