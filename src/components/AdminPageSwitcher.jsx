import React from 'react'
import { Link } from 'react-router-dom'

export default function AdminPageSwitcher(props) {
    return (
        <div className="flex justify-between align-middle w-4/12 ml-auto mr-28 mt-4">
        <Link to="/admin" className={`font-bold ${props.page==='campaign' ? 'text-tosca underline' : 'text-gray-500' }`}>All Campaigns</Link >
        <Link to="/admindonations"className={`font-bold ${props.page==='donations' ? 'text-tosca  underline' : 'text-gray-500' }`}>All Donations</Link >
        <Link to="/adminwithdrawal" className={`font-bold ${props.page==='withdrawal' ? 'text-tosca  underline' : 'text-gray-500' }`}>All Withdrawal</Link >
        {/* <input type="text" placeholder="search" className="border border-solid p-1 px-2 text-md"/> */}
      </div>
    )
}
