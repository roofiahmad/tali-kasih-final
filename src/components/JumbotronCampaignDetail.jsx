import React, { useState } from "react";
import ProgressBar from "./ProgressBar";
import { Link, useParams } from "react-router-dom";
import campaignImage from '../assets/campaign-image.png';
import 'react-toastify/dist/ReactToastify.css';
import ModalsCampaignDelete from "./ModalsCampaignDelete";


export default function JumbotronCampaignDetail(props) {
  const token = localStorage.getItem("token");
  let {campaignId} = useParams();
  const [dropdown, setDropdown] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  // function to add thousand separator value of IDR
  const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  const end = props.campaignData.due_date.split('T'); 
  const calc = end[0].split('-');
  const newDay = new Date();
  const today = [newDay.getFullYear(), newDay.getMonth()+1, newDay.getDate()];
  let dayRemain = (Number(calc[0])-today[0])*365 + (Number(calc[1])-today[1])*30 +(Number(calc[2])-today[2]);
  // pevent days remain have negative value
  if(dayRemain <0){
    dayRemain= 0;
  }

  const handleDelete = () => {
    setModalDelete(!modalDelete)
  }
 
  return (
    <div>
      {modalDelete ? <ModalsCampaignDelete  modalDelete={handleDelete}/> : null}
      <div className="flex justify-between w-full">
        <h2 className=" inline-block lg:text-3xl text-2xl my-4 lg:mt-10 mt-4 font-bold">
          {props.campaignData.title}
        </h2>
        {props.role === "fundraiser" ? 
        <div
          className="justify-self-center self-center"
          onClick={() => setDropdown(!dropdown)}
        >
          <i className="fas fa-cog text-2xl "></i>
          <i className="fas fa-caret-down text-xl mx-2"></i>
          {dropdown ? (
            <div className="absolute w-max p-2 -right-0 rounded-md z-10 bg-white shadow fromtop-animation ">
              <Link to={`/editcampaign/${props.campaignData.id}`}>
              <p className="lg:text-lg text-base campaign-text-setting text-md">Edit Campaign</p>
              </Link>
              <p onClick={handleDelete} className=" lg:text-lg text-base campaign-text-setting text-md">Delete Campaign</p>
            </div>
          ) : null}
        </div> : null}
      </div>
      <div className="flex flex-col md:flex-row lg:h-4/6">
        <img
          className="lg:w-8/12 md:w-full rounded"
          src={props.campaignData.images === 'https://talikasih.kuyrek.com:3001/img/' ?  campaignImage : props.campaignData.images}
          alt="Campaign Image"
        />
        <div className="lg:w-5/12 md:w-full rounded border-2 border-gray-200 lg:ml-6 lg:p-5 p-3 ">
          <div className="flex flex-col justify-between">
          <p className="text-red-700 lg:text-2xl text-base font-medium w-7/10 ">IDR {numberWithCommas(props.campaignData.total_donation_rupiah)}</p>
          <p className="lg:w-full text-md end text-gray-400 inline-block">{`IDR ${numberWithCommas((props.campaignData.goal - props.campaignData.total_donation_rupiah <= 0 ? 0 : props.campaignData.goal - props.campaignData.total_donation_rupiah))} remaining`}</p>
          </div>
          <ProgressBar totalDonation={props.campaignData.total_donation_rupiah} donationGoal={props.campaignData.goal} />
          <p>from IDR {numberWithCommas(props.campaignData.goal)} goal</p>
          <div className="fundraiser-profil flex gap-4 mt-1">
            <img
              src={props.campaignData.user.profile_image === 'https://talikasih.kuyrek.com:3000/img/null' ?  campaignImage : props.campaignData.user.profile_image}
              alt="fundraiser-image"
              className="w-12 h-12 inline-block border border-gray-500 rounded-md "
            />
            <div className="inline-block ">
              <p className="font-medium">{props.campaignData.user.name}</p>
              <p className="text-gray-400">Fundraiser</p>
            </div>
          </div>
          <div className="grid grid-cols-3 mt-5 text-center justify-items-center">
            <div className="col-span-1 lg:w-24 lg:h-24 w-20 h-20 border-2 rounded-md shadow-md lg:p-3 p-2">
              <p className="text-tosca lg:text-2xl text-xl">{dayRemain}</p>
              <p className="lg:text-base text-sm" >Days left</p>
            </div>
            <div className="col-span-1 lg:w-24 lg:h-24 w-20 h-20 border-2 rounded-md shadow-md lg:p-3 p-2">
              <p className="text-tosca lg:text-2xl text-xl">{props.campaignData.total_donation}</p>
              <p className="lg:text-base text-sm" >Donations</p>
            </div>
            <div className="col-span-1 lg:w-24 lg:h-24 w-20 h-20 border-2 rounded-md shadow-md lg:p-3 p-2">
              <p className="text-tosca lg:text-2xl text-xl">{props.campaignData.share < 1 ? '0': props.campaignData.total_share}</p>
              <p className="lg:text-base text-sm" >Share</p>
            </div>
          </div>
          <div className="grid grid-cols-1 my-2">
            <button 
            onClick={(e) => props.modalShareHandler(e.target.value)}
            className="btn-outline-red uppercase border-tosca">
              Share
            </button>
            { props.role === "fundraiser" ? <button
              onClick={(e) => props.newProgressModal(e.target.value)}
              className="btn-outline-red uppercase"
            > New Progress
            </button>
            :
            <Link
            to={`/createdonation/${campaignId}`}
            className="btn-outline-red uppercase text-center"
            > 
            Donate
            </Link>
            }
          </div>
        </div>
      </div>
    </div>
  );
}
