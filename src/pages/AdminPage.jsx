import React, { useState, useEffect } from 'react';
import AllCampaign from '../components/AllCampaign';
import CategoryButtonAdmin from '../components/CategoryButtonAdmin';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import axios from 'axios';
import PaginationComp from '../components/PaginationComp';
import AdminPageSwitcher from '../components/AdminPageSwitcher';
import SpinnerPage from '../components/SpinnerPage';

const AdminPage = () => {
  const [loading, setLoading] = useState(true)
  const [errorFetch, setErrorFetch] = useState(false)
  const token = localStorage.getItem("token")
  let history = useHistory();
  const [role] = useState(localStorage.getItem("role"));
  const [campaignData,setCampaignData] = useState([]);
  const [campaignByCategory,setcampaignByCategory] = useState([])
  const [campaignByPage, setCampaignByPage] = useState([])
  
  const dropdownHandler = (id)=> {
    let copyCampaignData = [...campaignByPage];
    copyCampaignData[id].dropdown = !campaignByPage[id].dropdown;
    setCampaignByPage(copyCampaignData)
  }

  const updateType = (type,id)=> {
    updateStatusCampaign(type,id)
  }

  useEffect(() => {
    kick();
    getAllCampaign();
    
  }, []);
  
  useEffect(() => {
    handlePagination()
  }, [campaignByCategory])

  const getAllCampaign = (category) => {
    let url = `https://talikasih.kuyrek.com:3001/campaign/byadmin?page=1&limit=100`;
    const config = {
      headers: {
        'Authorization': 'Bearer ' + token, 
      },
    };
    axios.get(url,config)
    .then((response) => {
      setLoading(false)
      setCampaignData(response.data.posts);
      response.data.posts.forEach(element => {
        element.dropdown= false;      
      });
      setcampaignByCategory(response.data.posts)
      setCampaignByPage(response.data.posts.slice(0,9))
    })
    .catch((err)=>{
      setLoading(false);
      setErrorFetch(true)
    })
  }

  const updateStatusCampaign = (type, idcampaign) =>{
    let updateType = {
      status:type
    }
    let url = `https://talikasih.kuyrek.com:3001/campaign/update/status/${idcampaign}`;
    const config = {
      headers: {
        'Authorization': 'Bearer ' + token, 
      },
    };
    axios.put(url,updateType,config)
    .then((response) =>{
      toast.success("Campaign status updated successfully!");
      window.location.reload()
    })
    .catch((err) => {
      toast.error("Campaign status updated failed!");
      window.location.reload()
    })
  }

  const filterByCategory = (category) =>{
    setcampaignByCategory(campaignData.filter((item)=>{
      return item.category ===category
    }))
  }
  const kick = () => {
    if (role !== "admin") {
      toast.error("Sorry, you are not an admin!", {
        position : toast.POSITION.TOP_CENTER
      });
      history.push("/");
    }
  }

  const handlePagination = (indexAwal , indexAkhir ) => {
    let copyArray = [...campaignByCategory];
    let slicedArray = copyArray.slice(indexAwal, indexAkhir);
    setCampaignByPage(slicedArray)
}


return (
  loading ? 
  <SpinnerPage/>
  :
  campaignData.length>0  && loading === false && errorFetch === false ?(    <div>
    <AdminPageSwitcher page={'campaign'}/> 
    <CategoryButtonAdmin filterByCategory={filterByCategory}/>
    <AllCampaign campaignByPage={campaignByPage}  dropdownHandler={dropdownHandler} updateType={updateType}/>
    <PaginationComp campaignData={campaignByCategory} pagination={handlePagination} />
  </div>)
  : errorFetch=== true && loading === false ?
  <div>
  <AdminPageSwitcher page={'campaign'}/> 
  <div className="h-3/6 flex flex-wrap content-center justify-center">
    <h1 className="text-3xl text-tosca font-bold">Oops.. Something Went Wrong</h1>
    <span className="ml-2 animate-bounce text-3xl text-tosca font-bold">._.</span>
  </div>
  </div>
  : null
)
}

export default AdminPage;


