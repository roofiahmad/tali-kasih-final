import React, { useState, useEffect } from "react";
import CategoryButtonAdmin from "../components/CategoryButtonAdmin";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";
import AllWithdrawal from "../components/AllWithdrawal";
import AdminPageSwitcher from "../components/AdminPageSwitcher";
import SpinnerPage from "../components/SpinnerPage";

export default function AdminWithdrawal() {
  const [loading, setLoading] = useState(true);
  const [errorFetch, setErrorFetch] = useState(false);
  const token = localStorage.getItem("token");
  let history = useHistory();
  const [role] = useState(localStorage.getItem("role"));
  const [campaignData, setCampaignData] = useState([]);
  const [filteredCampaignData, setFilteredCampaignData] = useState([]);

  const dropdownHandler = (id) => {
    let copyCampaignData = [...filteredCampaignData];
    copyCampaignData[id].dropdown = !filteredCampaignData[id].dropdown;
    setFilteredCampaignData(copyCampaignData);
  };

  const updateVerified = (verified, id) => {
    updateStatusCampaign(verified, id);
  };

  useEffect(() => {
    kick();
    getAllWithdrawal();
  }, []);

  const getAllWithdrawal = (category) => {
    let url = "https://talikasih.kuyrek.com:3003/update/get/withdraw";
    const config = {
      headers: {
        Authorization: "Bearer " + token,
      },
    };
    axios
      .get(url, config)
      .then((response) => {
        setLoading(false);
        setCampaignData(response.data.data);
        response.data.data.forEach((element) => {
          element.dropdown = false;
        });
        setFilteredCampaignData(response.data.data);
      })
      .catch((err) => {
        setLoading(false);
        setErrorFetch(true);
      });
  };

  const updateStatusCampaign = (verified, iddonation) => {
    let updateVerified = {
      isVerified: verified,
    };
    let url = `https://talikasih.kuyrek.com:3002/donation/update/verified/${iddonation}`;
    const config = {
      headers: {
        Authorization: "Bearer " + token,
      },
    };
    axios
      .put(url, updateVerified, config)
      .then((response) => {
        toast.success("Campaign verified updated successfully!");
        window.location.reload();
      })
      .catch((err) => {
        toast.success("Campaign updated failed!");
      });
  };

  const filterByCategory = (category) => {
    setFilteredCampaignData(
      campaignData.filter((item) => {
        return item.campaign.category === category;
      })
    );
  };

  const kick = () => {
    if (role !== "admin") {
      toast.error("Sorry, you are not an admin!", {
        position: toast.POSITION.TOP_CENTER,
      });
      history.push("/");
    }
  };

  return loading ? (
    <SpinnerPage />
  ) : campaignData.length > 0 && loading === false && errorFetch === false ? (
    <div>
      <AdminPageSwitcher page={"withdrawal"} />
      <CategoryButtonAdmin filterByCategory={filterByCategory} />
      <AllWithdrawal
        filteredCampaign={filteredCampaignData}
        dropdownHandler={dropdownHandler}
        updateVerified={updateVerified}
      />
    </div>
  ) : errorFetch === true && loading === false ? (
    <div>
      <AdminPageSwitcher page={"withdrawal"} />
      <div className="h-3/6 flex flex-wrap content-center justify-center">
        <h1 className="text-3xl text-tosca font-bold">
          Oops.. Something Went Wrong
        </h1>
        <span className="ml-2 animate-bounce text-3xl text-tosca font-bold">
          ._.
        </span>
      </div>
    </div>
  ) : null;
}
